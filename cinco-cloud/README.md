<div align='center'>

<br />

<img src="https://gitlab.com/scce/cinco-cloud/-/raw/main/docs/vuepress/src/.vuepress/public/assets/cinco-cloud-logo.png" width="10%" alt="Cinco Cloud Logo" />

<h2>CINCO CLOUD - ONLINE PLATORM</h2>

</div>

## Description

The framework application that runs the cloud instance of Cinco. Takes care of things like browser GUI, user account, organization and project management. 

## Documentation

Indepth Documentation is under construction and will be found as part of our [website](https://scce.gitlab.io/cinco-cloud/).

## Used Technologies

[Angular/TS][angular] - Frontend language.

[Java][java] - Backend language.

[Quarkus][quarkus] - Backend framework

[//]: # "Source definitions"
[angular]: https://angular.io/ "Angular"
[java]: https://www.java.com/de/ "Java"
[quarkus]: https://quarkus.io/ "Quarkus"

## License

[EPL2](https://www.eclipse.org/legal/epl-2.0/)
