package info.scce.cincocloud.core.rest.tos;

public enum ProjectDeploymentStatus {
  DEPLOYING,
  READY,
  TERMINATING,
  FAILED
}
