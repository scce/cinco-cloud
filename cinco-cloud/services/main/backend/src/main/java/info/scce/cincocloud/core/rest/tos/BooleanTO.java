package info.scce.cincocloud.core.rest.tos;

public class BooleanTO {

  public boolean value;

  public BooleanTO(boolean value) {
    this.value = value;
  }

}
