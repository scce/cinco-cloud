package info.scce.cincocloud.k8s.shared;

public enum EditorType {
    MODEL_EDITOR,
    LANGUAGE_EDITOR;

    public static final String KEY = "EDITOR_TYPE";
}