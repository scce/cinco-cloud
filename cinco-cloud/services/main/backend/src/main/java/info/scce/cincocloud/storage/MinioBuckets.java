package info.scce.cincocloud.storage;

public class MinioBuckets {

  public final static String PROJECTS_KEY = "projects";
  public final static String BUILD_JOB_LOGS_KEY = "build-jobs-logs";
  public final static String FILE_UPLOADS_KEY = "file-uploads";
}
