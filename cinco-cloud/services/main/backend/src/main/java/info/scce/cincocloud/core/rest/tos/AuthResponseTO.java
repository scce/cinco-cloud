package info.scce.cincocloud.core.rest.tos;

public class AuthResponseTO {

  public String token;

  public AuthResponseTO() {
  }

  public AuthResponseTO(final String token) {
    this.token = token;
  }
}
