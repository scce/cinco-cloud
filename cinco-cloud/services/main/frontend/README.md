<div align='center'>

<br />

<img src="https://gitlab.com/scce/cinco-cloud/-/raw/main/docs/vuepress/src/.vuepress/public/assets/cinco-cloud-logo.png" width="10%" alt="Cinco Cloud Logo" />

<h2>CINCO CLOUD - FRONTEND</h2>

</div>

## Contents

The frontend project for the Cinco Cloud plattform.

Provides user, team, and project management. Working on Cinco projects from the Cinco Cloud plattform uses the editor provided in the [Archetype](https://gitlab.com/scce/cinco-cloud/-/tree/main/cinco-cloud-archetype/).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

## Documentation

Indepth Documentation is under construction and will be found as part of our [website](https://scce.gitlab.io/cinco-cloud/).

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Used Technologies

[Angular/TS][angular] - Frontend language.

[//]: # "Source definitions"
[angular]: https://angular.io/ "Angular"

## License

[EPL2](https://www.eclipse.org/legal/epl-2.0/)
