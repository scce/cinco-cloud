export interface UpdateOrganizationInput {
  name: string;
  description?: string;
  logoId?: number;
}
