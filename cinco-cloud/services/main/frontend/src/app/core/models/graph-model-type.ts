import { BaseEntity } from './base-entity';

export class GraphModelType extends BaseEntity {
  typeName: string;
  fileExtension: string;
}
