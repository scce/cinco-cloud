export interface UpdateWorkspaceImageInput {
  published: boolean
  featured?: boolean
}
