import { FileReference } from '../file-reference';

export class UpdateCurrentUserProfilePictureInput {
  profilePicture: FileReference;
}
