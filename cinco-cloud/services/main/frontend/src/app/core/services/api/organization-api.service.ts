import { Injectable } from '@angular/core';
import { BaseApiService } from './base-api.service';
import { HttpClient } from '@angular/common/http';
import { Organization } from '../../models/organization';
import { map, Observable } from 'rxjs';
import { User } from '../../models/user';
import { fromJsog, fromJsogList, toJsog } from '../../utils/jsog-utils';
import { BooleanResponse } from "../../models/boolean-response";
import { Project } from "../../models/project";
import { Page } from '../../models/page';
import { UpdateOrganizationInput } from '../../models/forms/update-organization-input';

@Injectable({
  providedIn: 'root'
})
export class OrganizationApiService extends BaseApiService {

  constructor(http: HttpClient) {
    super(http);
  }

  public getAll(): Observable<Organization[]> {
    return this.http.get(`${this.apiUrl}/organizations`, this.defaultHttpOptions).pipe(
      map((body: any) => this.transformPage(body).items)
    );
  }

  public getAllPaged(page: number, size: number): Observable<Page<Organization>> {
    const options = {
      ...this.defaultHttpOptions,
      params: { page, size }
    };

    return this.http.get(`${this.apiUrl}/organizations`, options).pipe(
      map((body: any) => this.transformPage(body))
    );
  }

  public get(organizationId: number): Observable<Organization> {
    return this.http.get(`${this.apiUrl}/organizations/${organizationId}`, this.defaultHttpOptions).pipe(
      map(body => this.transformSingle(body))
    );
  }

  public create(organization: Organization): Observable<Organization> {
    return this.http.post(`${this.apiUrl}/organizations`, toJsog(organization), this.defaultHttpOptions).pipe(
      map(body => this.transformSingle(body))
    );
  }

  public update(organization: Organization, input: UpdateOrganizationInput): Observable<Organization> {
    return this.http.put(`${this.apiUrl}/organizations/${organization.id}`, input, this.defaultHttpOptions).pipe(
      map(body => this.transformSingle(body))
    );
  }

  public delete(organization: Organization): Observable<Organization> {
    return this.http.delete(`${this.apiUrl}/organizations/${organization.id}`, this.defaultHttpOptions).pipe(
      map(body => organization)
    );
  }

  public leave(organization: Organization): Observable<Organization> {
    return this.http.put(`${this.apiUrl}/organizations/${organization.id}/rpc/leave`, null, this.defaultHttpOptions).pipe(
      map(body => organization)
    );
  }

  public addOwner(organization: Organization, user: User): Observable<Organization> {
    return this.http.post(`${this.apiUrl}/organizations/${organization.id}/owners`, { userId: user.id }, this.defaultHttpOptions).pipe(
      map(body => this.transformSingle(body))
    );
  }

  public addMember(organization: Organization, user: User): Observable<Organization> {
    return this.http.post(`${this.apiUrl}/organizations/${organization.id}/members`, { userId: user.id }, this.defaultHttpOptions).pipe(
      map(body => this.transformSingle(body))
    );
  }

  public removeUser(organization: Organization, user: User): Observable<Organization> {
    return this.http.delete(`${this.apiUrl}/organizations/${organization.id}/users/${user.id}`, this.defaultHttpOptions).pipe(
      map(body => this.transformSingle(body))
    );
  }

  public hasActiveBuildJobs(organization: Organization): Observable<BooleanResponse> {
    return this.http.get(`${this.apiUrl}/organizations/${organization.id}/rpc/has-active-build-jobs`, this.defaultHttpOptions).pipe(
      map((body: any) => body as BooleanResponse)
    );
  }

  public createProject(newProject: Project): Observable<Project> {
    return this.http.post(`${this.apiUrl}/organizations/${newProject.organization.id}/projects`, toJsog(newProject), this.defaultHttpOptions).pipe(
      map(body => this.transformSingleProject(body))
    )
  }

  private transformSingleProject(body: any): Project {
    return fromJsog(body, Project);
  }

  private transformPage(body: any): Page<Organization> {
    return Page.fromObject(body, this.transformList(body.items));
  }

  private transformSingle(body: any): Organization {
    return fromJsog(body, Organization);
  }

  private transformList(body: any[]): Organization[] {
    return fromJsogList(body, Organization);
  }
}
