export interface UpdateProjectInput {
  name: string;
  description?: string;
  logoId?: number;
}
