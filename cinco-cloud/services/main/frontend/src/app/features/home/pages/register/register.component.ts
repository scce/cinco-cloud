import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserApiService } from '../../../../core/services/api/user-api.service';
import { SettingsApiService } from '../../../../core/services/api/settings-api.service';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { UserRegisterInput } from '../../../../core/models/forms/user-register-input';
import { ToastService, ToastType } from '../../../../core/services/toast.service';

@Component({
  selector: 'cc-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {

  public registerForm: UntypedFormGroup = new UntypedFormGroup({
    name: new UntypedFormControl('', [Validators.required]),
    username: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    password: new UntypedFormControl('', [Validators.required, Validators.minLength(5)]),
    passwordConfirm: new UntypedFormControl('', [Validators.required, Validators.minLength(5)])
  });

  public constructor(private router: Router,
                     private userApi: UserApiService,
                     private settingsApi: SettingsApiService,
                     private toastService: ToastService) {
  }

  public ngOnInit(): void {
    this.settingsApi.get().subscribe({
      next: settings => {
        if (!settings.allowPublicUserRegistration) {
          this.router.navigate(['/']);
        }
      },
      error: res => {
        this.toastService.show({
          type: ToastType.DANGER,
          message: `Could not fetch application settings.`
        });
        console.error(res.error.message);
      }
    });
  }

  public register(): void {
    const input: UserRegisterInput = this.registerForm.value;
    this.userApi.register(input).subscribe({
      next: registeredUser => {
        this.router.navigate(['/login']);
        let message = 'Your account has been created.';
        message += !registeredUser.activated ? ' Check your emails to activate your account.' : '';
        this.toastService.show({
          type: ToastType.SUCCESS,
          message
        });
      },
      error: res => {
        this.toastService.show({
          type: ToastType.DANGER,
          message: `Your account could not be created. ${res.error.message}`
        });
      }
    });
  }
}
