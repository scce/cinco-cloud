{
  "id": "3bbf5872-d2bc-48aa-a0ff-3f79d68d0c2c",
  "_containments": [
    {
      "id": "3279d6b0-956a-4857-bc79-4b68a5d12dc2",
      "type": "webstory:startmarker",
      "_attributes": {},
      "_size": {
        "width": 30,
        "height": 30
      },
      "_position": {
        "x": -15.109079935329248,
        "y": 278
      }
    },
    {
      "id": "4d709231-ba1f-4918-94b0-ef654cdc2a44",
      "type": "webstory:screen",
      "_attributes": {
        "backgroundImage": "webstory/images/cottage.jpg"
      },
      "_containments": [
        {
          "id": "8475f6ad-6593-49cb-b089-df2701f0e92c",
          "type": "webstory:rectangleclickarea",
          "_attributes": {},
          "_size": {
            "width": 181,
            "height": 128
          },
          "_position": {
            "x": 0,
            "y": 142
          }
        }
      ],
      "_size": {
        "width": 360,
        "height": 270
      },
      "_position": {
        "x": 143.40595774608005,
        "y": 134.8336571428481
      }
    },
    {
      "id": "7e731175-0f54-43a9-91d6-bb195f4609aa",
      "type": "webstory:screen",
      "_attributes": {
        "backgroundImage": "webstory/images/path.jpg"
      },
      "_containments": [
        {
          "id": "a1dd4f42-0ef5-4cd9-9b8a-21985795e497",
          "type": "webstory:ellipseclickarea",
          "_attributes": {},
          "_size": {
            "width": 74.94276118170963,
            "height": 104.6820289032801
          },
          "_position": {
            "x": 159.88743936429617,
            "y": 75.13434118837226
          }
        },
        {
          "id": "960d9a0d-fb94-4513-becc-f0c2e628fb08",
          "type": "webstory:rectangleclickarea",
          "_attributes": {},
          "_size": {
            "width": 125.47915608483339,
            "height": 137.60252253218505
          },
          "_position": {
            "x": 5.06496392588803,
            "y": 128.89024503531718
          }
        }
      ],
      "_size": {
        "width": 360,
        "height": 270
      },
      "_position": {
        "x": 616.2608874388525,
        "y": 230.26088743885254
      }
    },
    {
      "id": "264678ec-95a1-4993-8690-7b2277f166b6",
      "type": "webstory:screen",
      "_attributes": {
        "backgroundImage": "webstory/images/key.jpg"
      },
      "_containments": [
        {
          "id": "1dc2230c-198e-49e0-a7c2-0bf9580ecf95",
          "type": "webstory:rectangleclickarea",
          "_attributes": {},
          "_size": {
            "width": 49.09600802449852,
            "height": 160.3882387182054
          },
          "_position": {
            "x": 98.86285462195093,
            "y": 48.951289551059176
          }
        }
      ],
      "_size": {
        "width": 360,
        "height": 270
      },
      "_position": {
        "x": 164.23059872060475,
        "y": 545.4753749730502
      }
    },
    {
      "id": "0d30679f-d27c-4011-8c75-accf6ba8e0c4",
      "type": "webstory:screen",
      "_attributes": {
        "backgroundImage": "webstory/images/chest-no-key.jpg"
      },
      "_containments": [
        {
          "id": "dc90923c-0d3d-4fc4-8b64-06856080c56c",
          "type": "webstory:rectangleclickarea",
          "_attributes": {},
          "_size": {
            "width": 229.53343198032837,
            "height": 94.27904731577777
          },
          "_position": {
            "x": 66.80752997549098,
            "y": 91.92966030652869
          }
        }
      ],
      "_size": {
        "width": 360,
        "height": 270
      },
      "_position": {
        "x": 1690.549351672159,
        "y": 538.1332281874938
      }
    },
    {
      "id": "427c74a5-423f-4f06-bcf2-a2ec248106d2",
      "type": "webstory:screen",
      "_attributes": {
        "backgroundImage": "webstory/images/chest.jpg"
      },
      "_containments": [
        {
          "id": "6a92282a-5668-425e-99a3-e840bfe64d1d",
          "type": "webstory:rectangleclickarea",
          "_attributes": {},
          "_size": {
            "width": 143.15804352819507,
            "height": 96.3457169805678
          },
          "_position": {
            "x": 93.3773885983247,
            "y": 123.09064291927223
          }
        },
        {
          "id": "0eeae339-123d-4dd7-9a07-a97caaebe779",
          "type": "webstory:rectangleclickarea",
          "_attributes": {},
          "_size": {
            "width": 103.3769071818679,
            "height": 189.9804094126556
          },
          "_position": {
            "x": 256.1590450502739,
            "y": 6.584036659454512
          }
        }
      ],
      "_size": {
        "width": 360,
        "height": 270
      },
      "_position": {
        "x": 1088.9722032746708,
        "y": 217.93981563365114
      }
    },
    {
      "id": "c4c1609f-0e33-461f-a5be-2bb55394bfca",
      "type": "webstory:screen",
      "_attributes": {
        "backgroundImage": "webstory/images/coins.jpg"
      },
      "_containments": [],
      "_size": {
        "width": 360,
        "height": 270
      },
      "_position": {
        "x": 1665.3914506688557,
        "y": 42.20937215023504
      }
    },
    {
      "id": "c3c640c0-edca-4235-b613-73b1bb529ab8",
      "type": "webstory:variable",
      "_attributes": {
        "name": "foundKey"
      },
      "_size": {
        "width": 138.6124437792314,
        "height": 129.90936483599077
      },
      "_position": {
        "x": 937.6296037061956,
        "y": 585.4978107771577
      }
    },
    {
      "id": "993eb448-1526-4a1d-a3a4-d80683f796f5",
      "type": "webstory:condition",
      "_attributes": {},
      "_size": {
        "width": 69.08368236956026,
        "height": 57.205092137431386
      },
      "_position": {
        "x": 1346.1469571771809,
        "y": 614.2809836085097
      }
    },
    {
      "id": "4faee037-eb1e-4adf-86cd-7e6191e95ed1",
      "type": "webstory:modifyvariable",
      "_attributes": {
        "value": true
      },
      "_size": {
        "width": 60,
        "height": 30
      },
      "_position": {
        "x": 768.9303184170994,
        "y": 741.6580708310978
      }
    },
    {
      "id": "9794b893-5a6c-4b22-854d-530fb468c97f",
      "type": "webstory:condition",
      "_attributes": {},
      "_size": {
        "width": 77.02972006553652,
        "height": 75.97526438897522
      },
      "_position": {
        "x": 651.0624722777887,
        "y": 560.4978098771387
      }
    },
    {
      "id": "e64c4465-3347-469d-a94a-ccea52ae0184",
      "type": "webstory:modifyvariable",
      "_attributes": {
        "value": "false"
      },
      "_size": {
        "width": 60,
        "height": 30
      },
      "_position": {
        "x": 497.2764035970386,
        "y": 465.77980275944947
      }
    }
  ],
  "_edges": [
    {
      "id": "229b87a7-0435-4f87-beba-36827d21e09d",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "3279d6b0-956a-4857-bc79-4b68a5d12dc2",
      "targetID": "4d709231-ba1f-4918-94b0-ef654cdc2a44",
      "_routingPoints": []
    },
    {
      "id": "984988a5-2cad-42b5-ad26-ac990ea46a90",
      "type": "webstory:dataflow",
      "_attributes": {},
      "sourceID": "c3c640c0-edca-4235-b613-73b1bb529ab8",
      "targetID": "993eb448-1526-4a1d-a3a4-d80683f796f5",
      "_routingPoints": []
    },
    {
      "id": "ca6d4708-ebd1-409a-8820-07526449ee2e",
      "type": "webstory:dataflow",
      "_attributes": {},
      "sourceID": "4faee037-eb1e-4adf-86cd-7e6191e95ed1",
      "targetID": "c3c640c0-edca-4235-b613-73b1bb529ab8",
      "_routingPoints": []
    },
    {
      "id": "84e18669-bba4-4cf4-9f99-b1ac1ba2e314",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "1dc2230c-198e-49e0-a7c2-0bf9580ecf95",
      "targetID": "4faee037-eb1e-4adf-86cd-7e6191e95ed1",
      "_routingPoints": []
    },
    {
      "id": "667ef0b0-1b91-4cde-a3cb-261a28641e62",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "4faee037-eb1e-4adf-86cd-7e6191e95ed1",
      "targetID": "7e731175-0f54-43a9-91d6-bb195f4609aa",
      "_routingPoints": []
    },
    {
      "id": "18b36cec-47d9-4ea3-b70f-6775d8fda2e4",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "960d9a0d-fb94-4513-becc-f0c2e628fb08",
      "targetID": "9794b893-5a6c-4b22-854d-530fb468c97f",
      "_routingPoints": []
    },
    {
      "id": "2aeb50ce-7c61-407b-a4d2-a5a0f4e77871",
      "type": "webstory:dataflow",
      "_attributes": {},
      "sourceID": "c3c640c0-edca-4235-b613-73b1bb529ab8",
      "targetID": "9794b893-5a6c-4b22-854d-530fb468c97f",
      "_routingPoints": []
    },
    {
      "id": "9e6eaebe-c712-4ffe-b0e0-09a0665c4dfd",
      "type": "webstory:ttransition",
      "_attributes": {},
      "sourceID": "9794b893-5a6c-4b22-854d-530fb468c97f",
      "targetID": "7e731175-0f54-43a9-91d6-bb195f4609aa",
      "_routingPoints": []
    },
    {
      "id": "875efa58-923d-41ed-9c03-857c3e5785a7",
      "type": "webstory:ftransition",
      "_attributes": {},
      "sourceID": "9794b893-5a6c-4b22-854d-530fb468c97f",
      "targetID": "264678ec-95a1-4993-8690-7b2277f166b6",
      "_routingPoints": []
    },
    {
      "id": "3369b0bc-88a9-4a71-bba7-4eb28f8e62ac",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "6a92282a-5668-425e-99a3-e840bfe64d1d",
      "targetID": "993eb448-1526-4a1d-a3a4-d80683f796f5",
      "_routingPoints": []
    },
    {
      "id": "589bfdcb-b4ff-420f-ab08-b726730da4da",
      "type": "webstory:ttransition",
      "_attributes": {},
      "sourceID": "993eb448-1526-4a1d-a3a4-d80683f796f5",
      "targetID": "c4c1609f-0e33-461f-a5be-2bb55394bfca",
      "_routingPoints": []
    },
    {
      "id": "fbc4aebc-6dc0-4b6d-bac9-4537f0cdc3a4",
      "type": "webstory:ftransition",
      "_attributes": {},
      "sourceID": "993eb448-1526-4a1d-a3a4-d80683f796f5",
      "targetID": "0d30679f-d27c-4011-8c75-accf6ba8e0c4",
      "_routingPoints": []
    },
    {
      "id": "6702f3be-f57f-4391-b91e-c8776cce4a18",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "dc90923c-0d3d-4fc4-8b64-06856080c56c",
      "targetID": "427c74a5-423f-4f06-bcf2-a2ec248106d2",
      "_routingPoints": []
    },
    {
      "id": "63d9d691-809f-4c80-a9c9-3386b6983895",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "0eeae339-123d-4dd7-9a07-a97caaebe779",
      "targetID": "7e731175-0f54-43a9-91d6-bb195f4609aa",
      "_routingPoints": []
    },
    {
      "id": "9b677b1c-ab44-4c69-b743-1c8b9090984f",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "a1dd4f42-0ef5-4cd9-9b8a-21985795e497",
      "targetID": "427c74a5-423f-4f06-bcf2-a2ec248106d2",
      "_routingPoints": []
    },
    {
      "id": "5592b211-f613-4499-9bed-948a09dfeeed",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "8475f6ad-6593-49cb-b089-df2701f0e92c",
      "targetID": "e64c4465-3347-469d-a94a-ccea52ae0184",
      "_routingPoints": []
    },
    {
      "id": "a869df37-de00-4755-b7d6-4d4df00bcfc5",
      "type": "webstory:transition",
      "_attributes": {},
      "sourceID": "e64c4465-3347-469d-a94a-ccea52ae0184",
      "targetID": "7e731175-0f54-43a9-91d6-bb195f4609aa",
      "_routingPoints": []
    },
    {
      "id": "55ad0706-75a0-4fcb-8aa6-dfa9645c94ae",
      "type": "webstory:dataflow",
      "_attributes": {},
      "sourceID": "e64c4465-3347-469d-a94a-ccea52ae0184",
      "targetID": "c3c640c0-edca-4235-b613-73b1bb529ab8",
      "_routingPoints": []
    }
  ],
  "type": "webstory:webstory",
  "_sourceUri": "/Users/sami/Desktop/rebase/cinco-cloud/cinco-cloud-archetype/editor/workspace/example-models/example.story",
  "_attributes": {}
}