{
  "id": "85c279d4-0412-4b6c-b64f-55edb5328b48",
  "_containments": [
    {
      "id": "4490eeb9-9465-40b7-8f30-cbc577b6b394",
      "type": "metamodel:type",
      "_attributes": {
        "Name": "",
        "documentation": "This is some documentation\n\n",
        "name": "Product"
      },
      "_containments": [],
      "_size": {
        "width": 100,
        "height": 50
      },
      "_position": {
        "x": 278.79980053986276,
        "y": -71.16212538469662
      }
    },
    {
      "id": "6d0ba394-dfe8-4df5-981f-e4db78f85bfe",
      "type": "metamodel:type",
      "_attributes": {
        "name": "Komponenten",
        "documentation": "This is some documentation.\n"
      },
      "_containments": [],
      "_size": {
        "width": 155,
        "height": 56
      },
      "_position": {
        "x": 87.91357319839385,
        "y": 85.03529176016681
      }
    },
    {
      "id": "a67e0255-180d-4b35-8cad-120845f1ca93",
      "type": "metamodel:type",
      "_attributes": {
        "name": "Materialien",
        "documentation": ""
      },
      "_containments": [],
      "_size": {
        "width": 100,
        "height": 50
      },
      "_position": {
        "x": 114.25822873488283,
        "y": 207.18871336510085
      }
    },
    {
      "id": "7c13d8af-746e-4a71-8b8f-5c6551bfd9d5",
      "type": "metamodel:type",
      "_attributes": {
        "name": "Inhaltsstoffe",
        "documentation": ""
      },
      "_containments": [
        {
          "id": "a45646c2-2a53-40e3-9eb7-8ba99ec89fac",
          "type": "metamodel:type",
          "_attributes": {
            "name": "Farbe",
            "documentation": ""
          },
          "_containments": [],
          "_size": {
            "width": 126.01060692517117,
            "height": 38.69104046731685
          },
          "_position": {
            "x": 21.995020107452046,
            "y": 142.1025014069431
          }
        },
        {
          "id": "11d96d6b-4e90-429f-a34f-287d6873f992",
          "type": "metamodel:type",
          "_attributes": {
            "name": "Additive",
            "documentation": ""
          },
          "_containments": [],
          "_size": {
            "width": 130.53419073824438,
            "height": 39.82193642058517
          },
          "_position": {
            "x": 18.181022623574716,
            "y": 88.81735868438383
          }
        },
        {
          "id": "5351131c-1366-4a9d-8d99-6313d35cd14b",
          "type": "metamodel:type",
          "_attributes": {
            "name": "Kunststoff",
            "documentation": ""
          },
          "_containments": [],
          "_size": {
            "width": 128.2723988317079,
            "height": 36.42924856078025
          },
          "_position": {
            "x": 18.60233224764705,
            "y": 37.79400786836124
          }
        }
      ],
      "_size": {
        "width": 161.91123300904331,
        "height": 197.01647392488064
      },
      "_position": {
        "x": 81.68701633209798,
        "y": 326.97394702449594
      }
    },
    {
      "id": "765e0319-777a-45a3-8c00-60f4b7818821",
      "type": "metamodel:type",
      "_attributes": {
        "name": "Eigenschaft",
        "documentation": ""
      },
      "_containments": [],
      "_size": {
        "width": 100,
        "height": 50
      },
      "_position": {
        "x": 457.54341219791223,
        "y": 76.88851127629455
      }
    },
    {
      "id": "5a945684-6402-47be-860a-cf556573ab22",
      "type": "metamodel:type",
      "_attributes": {
        "name": "Technische Anforderung",
        "documentation": ""
      },
      "_containments": [
        {
          "id": "741c45e4-494f-44d5-aed1-4b64eddea7fd",
          "type": "metamodel:type",
          "_attributes": {
            "name": "Temperatur",
            "documentation": ""
          },
          "_containments": [],
          "_size": {
            "width": 100,
            "height": 50
          },
          "_position": {
            "x": 48.6092693647563,
            "y": 99.33217113488269
          }
        },
        {
          "id": "91398acc-57b1-4849-aaee-120e2e2a5c6d",
          "type": "metamodel:type",
          "_attributes": {
            "name": "...",
            "documentation": ""
          },
          "_containments": [],
          "_size": {
            "width": 100,
            "height": 50
          },
          "_position": {
            "x": 51.14116816150863,
            "y": 160.66589221116004
          }
        },
        {
          "id": "230fcf16-0c33-443c-8db4-86a01522cf49",
          "type": "metamodel:type",
          "_attributes": {
            "name": "Härte",
            "documentation": ""
          },
          "_containments": [],
          "_size": {
            "width": 100,
            "height": 50
          },
          "_position": {
            "x": 46.67229129119784,
            "y": 35.279052984223824
          }
        }
      ],
      "_size": {
        "width": 190.61431209065825,
        "height": 223.589687426474
      },
      "_position": {
        "x": 303.7759382869866,
        "y": 215.03368443274624
      }
    },
    {
      "id": "e054b75e-466f-4341-8287-c9b0851d0a5e",
      "type": "metamodel:type",
      "_attributes": {
        "name": "Regulatorische Anforderungen",
        "documentation": ""
      },
      "_containments": [],
      "_size": {
        "width": 167.25615753924345,
        "height": 38.61818872412803
      },
      "_position": {
        "x": 520.663256533648,
        "y": 213.99897431675785
      }
    },
    {
      "id": "1429d5e0-a71e-4167-962b-322a419cb090",
      "type": "metamodel:type",
      "_attributes": {
        "name": "Regularie1",
        "documentation": ""
      },
      "_containments": [],
      "_size": {
        "width": 100,
        "height": 50
      },
      "_position": {
        "x": 554.5224774047767,
        "y": 315.40056568361734
      }
    },
    {
      "id": "7d84efa0-1ac5-4610-9779-89b3c6eb16e7",
      "type": "metamodel:type",
      "_attributes": {
        "name": "asd",
        "documentation": ""
      },
      "_containments": [],
      "_size": {
        "width": 100,
        "height": 50
      },
      "_position": {
        "x": 695.7219013543021,
        "y": 57.59497620899935
      }
    },
    {
      "id": "6a343fe3-bb4d-4b6e-bf2c-f0c824409ab9",
      "type": "metamodel:type",
      "_attributes": {
        "name": "s",
        "documentation": ""
      },
      "_containments": [],
      "_size": {
        "width": 100,
        "height": 50
      },
      "_position": {
        "x": 682.8302076593448,
        "y": 403.0724086031409
      }
    }
  ],
  "_edges": [
    {
      "id": "c40c9f35-16c8-4f0f-be8b-cc048bc9fb02",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "consistsOf"
      },
      "sourceID": "4490eeb9-9465-40b7-8f30-cbc577b6b394",
      "targetID": "6d0ba394-dfe8-4df5-981f-e4db78f85bfe",
      "_routingPoints": []
    },
    {
      "id": "58df12f9-9b04-4bda-bd56-9e260b459820",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "consistsOf",
        "documentation": ""
      },
      "sourceID": "6d0ba394-dfe8-4df5-981f-e4db78f85bfe",
      "targetID": "a67e0255-180d-4b35-8cad-120845f1ca93",
      "_routingPoints": []
    },
    {
      "id": "3eb141e8-3446-4a29-beac-af26fc12944c",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "consistsOf",
        "documentation": ""
      },
      "sourceID": "a67e0255-180d-4b35-8cad-120845f1ca93",
      "targetID": "7c13d8af-746e-4a71-8b8f-5c6551bfd9d5",
      "_routingPoints": []
    },
    {
      "id": "805dcc37-fca6-4e49-926f-cc968c84d7a8",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "satisfies",
        "documentation": ""
      },
      "sourceID": "4490eeb9-9465-40b7-8f30-cbc577b6b394",
      "targetID": "765e0319-777a-45a3-8c00-60f4b7818821",
      "_routingPoints": []
    },
    {
      "id": "dfb710f4-c31b-4476-bdd5-5f59b328e977",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "isA",
        "documentation": ""
      },
      "sourceID": "5a945684-6402-47be-860a-cf556573ab22",
      "targetID": "765e0319-777a-45a3-8c00-60f4b7818821",
      "_routingPoints": []
    },
    {
      "id": "bbb8e042-a8dc-4008-8f25-2bfac955edb4",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "isA",
        "documentation": ""
      },
      "sourceID": "e054b75e-466f-4341-8287-c9b0851d0a5e",
      "targetID": "765e0319-777a-45a3-8c00-60f4b7818821",
      "_routingPoints": []
    },
    {
      "id": "9358877d-8511-446e-9b1f-021599a49d34",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "isA",
        "documentation": ""
      },
      "sourceID": "1429d5e0-a71e-4167-962b-322a419cb090",
      "targetID": "e054b75e-466f-4341-8287-c9b0851d0a5e",
      "_routingPoints": []
    },
    {
      "id": "c83e8012-1f7a-4a19-abba-7c1c0b895d93",
      "type": "metamodel:relation",
      "_attributes": {
        "Type": "relatesTo",
        "documentation": ""
      },
      "sourceID": "7d84efa0-1ac5-4610-9779-89b3c6eb16e7",
      "targetID": "6a343fe3-bb4d-4b6e-bf2c-f0c824409ab9",
      "_routingPoints": []
    }
  ],
  "type": "metamodel:model",
  "_sourceUri": "/Users/sami/Desktop/rebase/cinco-cloud/cinco-cloud-archetype/editor/workspace/example-models/example.mm",
  "_attributes": {}
}