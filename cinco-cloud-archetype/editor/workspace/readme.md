# Cinco Cloud Example Workspace

This Workspace contains all the example models and meta-models of Cinco Cloud.
These files can be used to test a the behavior of the general-purpose IME.
The `languages` folder contains the languages, i.e. metamodels and semantics, and the `example-models` folder the corresponding model files.

## Code of Conduct

Each language inside the `languages` folder should have its own folder.
How the language-files are structured inside that folder, is up to the language engineer.
