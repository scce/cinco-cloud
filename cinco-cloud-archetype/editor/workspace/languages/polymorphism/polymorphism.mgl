stylePath "styles.style"

graphModel polymorphismModel {
    diagramExtension "polymorphism"
    containableElements (GeneralTest, AbstractTest, RecursionTest, ListTest, KeepAttributesTest, ModelReferenceTest, SuperNode, SubNode1, SubNode2, SuperNodeAbstract, SubNodeAbstract1, SubNodeAbstract2)
}

node GeneralTest {
    style base("GeneralTest")
    attr string as primitiveTest
    attr SuperType as superTypeTest
    attr SubType1 as subType1Test
    attr SubType2 as subType2Test
}
node AbstractTest {
    style base("AbstractTest")
    attr SuperTypeAbstract as superTypeAbstractTest
    attr SubTypeAbstract1 as subTypeAbstract1Test
    attr SubTypeAbstract2 as subTypeAbstract2Test
}
node RecursionTest {
    style base("RecursionTest")
    attr SuperTypeRecursion as superTypeRecursionTest
}
node ListTest {
    style base("ListTest")
    attr SuperType as superTypeList[0, *]
    attr SuperTypeAbstract as superTypeAbstractList[0, *]
    attr SuperTypeRecursion as superTypeRecursionList[0, *]
}

node KeepAttributesTest {
    style base("KeepAttributesTest")

    attr KeepValuesCounterExample as keepAttrTest
}


node ModelReferenceTest {
    style base("ModelReferenceTest")

    attr SuperNode as superNodeReference
    attr SubNode1 as subNode1Reference
    // attr SuperNodeAbstract as superNodeAbstractReference // not supported yet
}


/*
    MODEL REFERENCE NODES
*/
node SuperNode {
    style base("SuperNode")
    attr string as name := "superNodeInstance"
}
node SubNode1 extends SuperNode {
    style base("SubNode1")
    override name := "subNode1Instance"
}
node SubNode2 extends SuperNode {
    style base("SubNode2")
    override name := "subNode2Instance"
}
abstract node SuperNodeAbstract {
    style base("SuperNodeAbstract")
    attr string as name := "superNodeInstance"
}
node SubNodeAbstract1 extends SuperNodeAbstract {
    style base("SubNodeAbstract1")
    override name := "subNodeAbstract1Instance"
}
node SubNodeAbstract2 extends SuperNodeAbstract {
    style base("SubNodeAbstract2")
    override name := "subNodeAbstract2Instance"
}


/*
    TYPES
*/
type SuperType {
    attr string as attrSuper
}
type SubType1 extends SuperType {
    attr string as attrSub1
}
type SubType2 extends SubType1 {
    attr string as attrSub2
}

abstract type SuperTypeAbstract {
    attr string as attrSuperAbstract
}
type SubTypeAbstract1 extends SuperTypeAbstract {
    attr string as attrSubAbstract1
}
type SubTypeAbstract2 extends SubTypeAbstract1 {
    attr string as attrSubAbstract2
}

type SuperTypeRecursion {
    attr string as superTypeRecursionAttr
}
type SubTypeRecursion extends SuperTypeRecursion {
    attr SuperTypeRecursion as recursiveAttr
}


/*
    Tests for keeping attribute values on type switch
*/
abstract type KeepValuesCounterExample {
    attr string as primitive
}
type KeepSub1 extends KeepValuesCounterExample {
    attr TypeOption1 as polymorphAttr
}
type KeepSub2 extends KeepValuesCounterExample {
    attr TypeOption2 as polymorphAttr
}
type TypeOption1 {
    attr string as A1
    attr string as A2
}
type TypeOption2 extends TypeOption1 {
    attr string as B1
    attr string as B2
}