/********************************************************************************
 * Copyright (c) 2023 Cinco Cloud and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
import { LanguageFilesRegistry, ValidationHandler } from '@cinco-glsp/cinco-glsp-api';
import { Action, ValidationResponseAction, ValidationRequestAction, ValidationStatus } from '@cinco-glsp/cinco-glsp-common';

/**
 * Language Designer defined example of a Validator
 */
export class ExampleValidator extends ValidationHandler {
    override CHANNEL_NAME: string | undefined = 'Validator Flowgraph [' + this.modelState.graphModel.id + ']';

    override execute(action: ValidationRequestAction, ...args: unknown[]): Promise<Action[]> | Action[] {
        // next actions
        return [
            ValidationResponseAction.create(
                this.modelState.graphModel.id,
                action.modelElementId,
                [
                    {
                        name: 'Test message',
                        message: 'This is a test message',
                        status: ValidationStatus.Info
                    }
                ],
                action.requestId
            )
        ];
    }

    override canExecute(action: ValidationRequestAction, ...args: unknown[]): Promise<boolean> | boolean {
        const element = this.getElement(action.modelElementId);
        return element !== undefined && element.type === 'graphmodel:flowgraph';
    }
}
// register into app
LanguageFilesRegistry.register(ExampleValidator);
