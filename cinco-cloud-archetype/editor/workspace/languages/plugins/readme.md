# Plugins Folder

You can place vscode extensions (*.vsix*) into this `plugins`-folder inside the `languages`-folder.
The plugins will then be installed into intialized model editor.
