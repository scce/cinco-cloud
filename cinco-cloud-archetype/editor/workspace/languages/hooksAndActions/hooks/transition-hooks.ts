/********************************************************************************
 * Copyright (c) 2024 Cinco Cloud.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 ********************************************************************************/
import { CreateEdgeOperation } from '@eclipse-glsp/server';
import { Edge, AbstractEdgeHook, LanguageFilesRegistry, Node } from '@cinco-glsp/cinco-glsp-api';
import { AssignValue, PropertyEditOperation } from '@cinco-glsp/cinco-glsp-common';

export class TransitionHooks extends AbstractEdgeHook {
    override CHANNEL_NAME: string | undefined = 'TransitionHooks [' + this.modelState.graphModel.id + ']';

    override canCreate(elementTypeId: string, source: Node, target: Node): boolean {
        this.log('Triggered preCreate. Can create edge of type (' + elementTypeId + ') for source (' + source.id + ') and target (' + target.id + ')');
        return true;
    }

    override preCreate(elementTypeId: string, source: Node, target: Node): void {
        this.log('Triggered preCreate. Creating edge of type (' + elementTypeId + ') for source (' + source.id + ') and target (' + target.id + ')');
    }

    override postCreate(edge: Edge): void {
        this.log('Triggered postCreate on edge (' + edge.id + ')');
    }

    override canDelete(edge: Edge): boolean {
        this.log('Triggered canDelete on edge (' + edge.id + ')');
        return true;
    }

    override preDelete(edge: Edge): void {
        this.log('Triggered preDelete on edge (' + edge.id + ')');
    }

    override postDelete(edge: Edge): void {
        this.log('Triggered postDelete on edge (' + edge.id + ')');
    }

    /**
     * Change Attribute
     */

    override canAttributeChange(edge: Edge, operation: PropertyEditOperation): boolean {
        this.log('Triggered canAttributeChange on edge (' + edge.id + ')');
        return operation.change.kind === 'assignValue';
    }

    override preAttributeChange(edge: Edge, operation: PropertyEditOperation): void {
        this.log('Triggered preAttributeChange on edge (' + edge.id + ')');
        this.log(
            'Changing: ' +
                operation.name +
                ' from: ' +
                edge.getProperty(operation.name) +
                ' to: ' +
                (AssignValue.is(operation.change) ? operation.change.value : 'undefined')
        );
    }

    override postAttributeChange(edge: Edge, attributeName: string, oldValue: any): void {
        this.log('Triggered postAttributeChange on edge (' + edge.id + ')');
        this.log('Changed: ' + attributeName + ' from: ' + oldValue + ' to: ' + edge.getProperty(attributeName));
    }

    /**
     * Select
     */

    override canSelect(edge: Edge, isSelected: boolean): boolean {
        this.log('Triggered canSelect on edge (' + edge.id + ')');
        return true;
    }

    override postSelect(edge: Edge, isSelected: boolean): boolean {
        this.log('Triggered postSelect on edge (' + edge.id + ')');
        return true;
    }

    /**
     * Double Click
     */

    override canDoubleClick(edge: Edge): boolean {
        this.log('Triggered canDoubleClick on edge (' + edge.id + ')');
        return true;
    }

    override postDoubleClick(edge: Edge): void {
        this.log('Triggered postDoubleClick on edge (' + edge.id + ')');
    }

    /**
     * Reconnect
     */

    override canReconnect(edge: Edge, newSource: Node, newTarget: Node): boolean {
        this.log('Triggered canReconnect on edge (' + edge.id + ')');
        this.log('Current Source (' + edge.source.id + ') - Current Target (' + edge.target.id + ')');
        this.log('New Source (' + newSource.id + ') - New Target (' + newTarget.id + ')');
        return true;
    }

    override preReconnect(edge: Edge, newSource: Node, newTarget: Node): void {
        this.log('Triggered canReconnect on edge (' + edge.id + ')');
        this.log('Current Source (' + edge.source.id + ') - Current Target (' + edge.target.id + ')');
        this.log('New Source (' + newSource.id + ') - New Target (' + newTarget.id + ')');
    }

    override postReconnect(edge: Edge, oldSource: Node, oldTarget: Node): void {
        this.log('Triggered postReconnect on edge (' + edge.id + ')');
        this.log('Old Source (' + oldSource.id + ') - Old Target (' + oldTarget.id + ')');
        this.log('Current Source (' + edge.source.id + ') - Current Target (' + edge.target.id + ')');
    }
}

LanguageFilesRegistry.register(TransitionHooks);
