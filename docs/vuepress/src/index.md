---
home: true
heroImage: /assets/cinco-cloud-logo.png
tagline: Documentation for developing, operating and using CincoCloud.
actionText: Get Started →
actionLink: /content/introduction/
footer: Made by LS5 CS TU Dortmund & Friends
---
